from app import app, models, db
import logging
from flask import render_template
from .Forms import BookForm, RegisterForm, ReadForm, LoginForm, ChangeForm
from datetime import datetime
import sqlite3
loggedonid = 0


@app.route('/', methods=['GET', 'POST'])
def base():
    form = LoginForm()
    if form.validate_on_submit():
        logging.info('Log in attempt')
        eusername = form.username.data
        epassword = form.password.data
        user = models.User.query.filter_by(username=eusername).first()
        if user.password == epassword:
            logging.info('attempt successful')
            loggedonid = user.id
            return render_template('loggedin.html', id = loggedonid)
    return render_template('login.html', title='Log in', form=form)

@app.route('/login.html', methods=['GET', 'POST'])
def main():
    form = LoginForm()
    if form.validate_on_submit():
        logging.info('Log in attempt')
        eusername = form.username.data
        epassword = form.password.data
        user = models.User.query.filter_by(username=eusername).first()
        if user.password == epassword:
            logging.info('attempt successful')
            loggedonid = user.id
            return redirect('/loggedin.html', id=loggedonid)
    return render_template('login.html', title='Log in', form=form)


@app.route('/book.html', methods=['GET', 'POST'])
def addbook():
    form = BookForm()
    if form.validate_on_submit():
        logging.info('book add attempt')
        datef = datetime.strptime(form.date.data, '%d/%m/%Y')
        book = models.Book(title = form.title.data, dateofpublishing = datef, author = form.author.data)
        db.session.add(book)
        db.session.commit()
        logging.info('book added')
    print(form.errors)
    return render_template('book.html',
                           title='Add Book',
                           form=form)
    #This function uses the AddForm described in forms.py to add a new record to the database using data the user inputs.

@app.route('/loggedin.html')
def loggedin():
    data = models.Read.query.all()
    booksread = []
    logging.info('Logged in successfully')
    for item in data:
        if item.user_id == loggedonid:
            logging.info('user-book relationship detected')
            book = models.Book.query.filter_by(id = item.book_id).first()
            booksread.append(book)
    return render_template('loggedin.html', id = loggedonid, data = booksread)

@app.route('/register.html', methods=['GET', 'POST'])
def register():
    form = RegisterForm()
    if form.validate_on_submit():
        datef = datetime.strptime(form.date.data, '%d/%m/%Y')
        user = models.User(username = form.username.data, dateofbirth = datef, password = form.password.data)
        db.session.add(user)
        db.session.commit()
        logging.info('User account registered')
        return render_template('login.html', title='Login', form=LoginForm())
    print(form.errors)
    return render_template('register.html',
                           title='Register',
                           form=form)

@app.route('/read.html', methods=['GET', 'POST'])
def read():
    form = ReadForm()
    if form.validate_on_submit():
        title = form.book.data
        book = models.Book.query.filter_by(title=title).first()
        datef = datetime.strptime(form.date.data, '%d/%m/%Y')
        bookid = book.id
        bookread = models.Read(user_id = loggedonid, book_id = bookid, dateread = datef)
        db.session.add(bookread)
        db.session.commit()
        logging.info('User-book relation added to database')
    return render_template('read.html',
                           title='Record Read',
                           form=form)

@app.route('/passchange.html', methods=['GET', 'POST'])
def change():
    form = ChangeForm()
    if form.validate_on_submit():
        logging.info('Password change attempt')
        new = form.new.data
        user = models.User.query.filter_by(id=loggedonid)
        if form.old.data == user.password:
            user.password = new
            db.session.commit()
            logging.info('Password changed successfully')
        else:
            logging.info('Old password incorrect')
    return render_template('passchange.html', title='change password', form=form)
