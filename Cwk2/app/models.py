from app import db
from sqlalchemy.orm import relationship, backref

class User(db.Model):
    __tablename__ = 'User'
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(100))
    password = db.Column(db.String(100))
    dateofbirth = db.Column(db.DateTime)
    book = relationship("Book", secondary="read")

class Book(db.Model):
    __tablename__ = 'Book'
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(100))
    author = db.Column(db.String(100))
    dateofpublishing = db.Column(db.DateTime)
    user = relationship("User", secondary="read")

class Read(db.Model):
    __tablename__ = 'read'
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('User.id'))
    book_id = db.Column(db.Integer, db.ForeignKey('Book.id'))
    dateread = db.Column(db.DateTime)
    user = relationship(User, backref=backref("Read", cascade = "all, delete-orphan"))
    book = relationship(Book, backref=backref("Read", cascade = "all, delete-orphan"))
