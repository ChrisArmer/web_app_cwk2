from flask_wtf import Form
from app import db, models
from wtforms import StringField, BooleanField, SubmitField, DateField, SelectField
from wtforms.validators import DataRequired

class BookForm(Form):
    title = StringField('title', validators=[DataRequired()])
    date = StringField('date in format dd/mm/yyyy', validators=[DataRequired()])
    author = StringField('author', validators=[DataRequired()])
    submit = SubmitField('Add Book')

class ReadForm(Form):
    booklist = []
    try:
        data = models.Book.query.all()
        for item in data:
            booklist.append(item.title)
        book = SelectField('Book:', choices=booklist)
        date = StringField('Date finished, Format as dd/mm/yyyy', validators=[DataRequired()])
        submit = SubmitField('I have read this book')
    except:
        print("error loading books")


class RegisterForm(Form):
    username = StringField('username', validators=[DataRequired()])
    password = StringField('password', validators=[DataRequired()])
    date = StringField('date of birth in format dd/mm/yyyy', validators=[DataRequired()])
    submit = SubmitField('Register')

class LoginForm(Form):
    username = StringField('username', validators=[DataRequired()])
    password = StringField('password', validators=[DataRequired()])
    submit = SubmitField('Login')

class ChangeForm(Form):
    old = StringField('Old Password', validators=[DataRequired()])
    new = StringField('New Password', validators=[DataRequired()])
    submit = SubmitField('Change Password')
